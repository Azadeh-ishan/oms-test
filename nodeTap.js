
'use strict'

const tap = require('tap')
const mam = require('../testTap.js')

// Always call as (found, wanted) by convention
function test () {
  tap.equal(mam(1), 'odd')
  tap.equal(mam(2), 'even')

  const t = require('tap')
  const myThing = require('./my-thing.js')

  t.equal(myThing.add(1, 2), 3, '1 added to 2 is 3')
  t.throws(() => myThing.add('dog', 'cat'), 'cannot add dogs and cats')
  t.equal(myThing.times(2, 2), 4, '2 times 2 is 4')
  t.equal(myThing.add(2, -1), 1, '2 added to -1 is 1')
  t.equal(myThing.times(-1, 3), 3, '-1 times 3 is -3')
  t.throws(() => myThing.times('best', 'worst'), 'can only times numbers')
}

module.exports = fp(test)