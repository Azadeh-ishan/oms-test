
const { promisify } = require('util')
const sleep = promisify(setTimeout)

const request = require('request-promise');

let user;
async function login (){

  var options = {
    method: 'POST',
    uri: `http://localhost:5020/oper/login`,
    body: {
      "username": "09121161998",
      "password": "123456",
    },
    json: true
  };

  try{
    const parsedBody = await request(options);
    if(parsedBody == 0){
      console.log(" parsedBody : not login");
    }else{
      console.log(" parsedBody : username :" +  parsedBody.username);
    }
    const token = parsedBody.token;
    console.log(" token :" +  token)
    console.log(" response :" +  parsedBody.response)
    user = token;
  } catch(err) {
    console.log(" err :" +  err);
  }

}

async function createPurchase (){

  var options = {
    method: 'POST',
    uri: 'http://localhost:5020/purchase/create',
    body: {
      // // "id":"b651ac8e-daf6-484c-bf1a-e5cdac694ac5",
      // // "purchaseNo":"05",
      // // "purchaseDate":"13990621",
      // // "benefCode":"0002",
      // //
      // "id": "b651ac8e-daf6-484c-bf1a-e5cdac694ac2",
      // "purchaseNo": "sh-327",
      // "purchaseDate": "2020-09-14 23:33:03",
      // "benefCode": "بازیکن",
      // "shippingCost": "81430",
      // "shippingCostRevenue": "74706",
      // "shippingCostTax": "4482",
      // "shippingCostToll": "2241",
      // "shippedBy": "Nilgam",
      // "cityName": "تهران",
      // "address": "م بهارستان خ مصطفی خمینی ک برزن ک املح القرا ک بیرونپور پ 19",
      // "mobile": "09127340378",
      // "contractOwnerName": "محمد امین جمشیدیان",
      // "contractNationalCode": "0017371384",
      // "contractTel": "0912769890",
      // "contractAddress": "پاسداران بالاتر از میدان اختیاریه خیابان اختیاریه شمالی کوچه غفاری کوچه گلستان پلاک ۵ زنگ ۱۰",
      //
      // // "articles":[
      // //     {"id":"b651ac8e-daf6-484c-bf1a-e5cdac694aa5" ,"purchaseId":"b651ac8e-daf6-484c-bf1a-e5cdac694ac5" , "goodsCode":"0010010001", "quantity":"10", "amount":"30000", "supplierCode":"0007", "purchaseArticlekind":"retail", "inventoryCode":"001"},
      // //     // {"id":"b651ac8e-daf6-484c-bf1a-e5cdac694ad9", "purchuseId":"b651ac8e-daf6-484c-bf1a-e5cdac694ad7" , "goodsCode":"2033", "quantity":"20", "amount":"600", "supplierCode":"0004", "purchuseArticlekind":"retail", "inventoryCode":"001"}
      // //  ],
      // "articles": [
      //   {
      //     "id": "b651ac8e-daf6-484c-bf1a-e5cdac694aa2",
      //     "purchaseId": "b651ac8e-daf6-484c-bf1a-e5cdac694ac2",
      //     "goodsCode": "8-1804-10",
      //     "quantity": "1",
      //     "amount": "600000",
      //     // "retailRevenue": "633028",
      //     // "retailTax": "46972",
      //     // "retailToll": "46972",
      //     "commissionPrice": "6000",
      //     "commissionRevenue": "5505",
      //     "commissionTax": "330",
      //     "commissionToll": "165",
      //     "supplierShipping": "30000",
      //     "supplierShippingRevenue": "27523",
      //     "supplierShippingTax": "1651",
      //     "supplierShippingToll": "826",
      //     "inboundFee": "10000",
      //     "inboundFeeRevenue": "9174",
      //     "inboundFeeTax": "550",
      //     "inboundFeeToll": "275",
      //     "dealName": "بازی فکری بازی کن مدل تاج و تخت Hand Of The King",
      //     "supplierCode": "10",
      //     "purchaseArticleKind": "marketPlace",
      //     "inventoryCode": "13579",
      //     "supplierPayment": " 554000"
      //   }
      // ]

    "id": "9",
      "purchaseNo": "sh-600",
      "purchaseDate": "2020-10-20 23:33:03",
      "benefCode": "5400",
      "shippingCost": "5000",
      "shippingCostRevenue": "0",
      "shippingCostTax": "0",
      "shippingCostToll": "0",
      "shippedBy": "post",
      "cityName": "tehran",
      "address": "tehran",
      "mobile": "09384784899",
      "contractOwnerName": "farzan karaji",
      "contractNationalCode": "0011756993",
      "contractTel": "02133226677",
      "contractAddress": "mirdamad",
      "articles": [
      {
        "goodsCode": "10010008",
        "quantity": "1",
        "amount": "380000",
        "supplierCode": "002",
        "purchaseArticleKind": "retail",
        "inventoryCode": "001",
        "retailRevenue": "0",
        "retailTax": "0",
        "retailToll": "0",
        "commissionPrice": "0",
        "commissionRevenue": "0",
        "commissionTax": "0",
        "commissionToll": "0",
        "supplierShipping": "30000",
        "supplierShippingRevenue": "15000",
        "supplierShippingTax": "3000",
        "supplierShippingToll": "33000",
        "inboundFee": "40000",
        "inboundFeeRevenue": "30000",
        "inboundFeeTax": "10000",
        "inboundFeeToll": "40000",
        "dealName": "test fani",
        "supplierPayment": "370000"
      }
    ]

    //     "id": "94",
    //     "purchaseNo" : "54",
    //     "purchaseDate":"8\/19\/20, 4:17 PM",
    //     "benefCode":"72",
    //     "shippingCost":"0",
    //     "shippingCostRevenue":"0",
    //     "shippingCostTax":"0",
    //     "shippingCostToll":"0",
    //     "shippedBy":"irpost",
    //     "cityName":"\u062a\u0647\u0631\u0627\u0646",
    //     "address":"\u062e\u06cc\u0627\u0628\u0627\u0646 \u0646\u06cc\u0627\u0648\u0631\u0627\u0646 \u06a9\u0648\u0686\u0647 \u06cc\u0627\u0633 \u067e\u0644\u0627\u06a9 \u06f9",
    //     "mobile":"09122943981",
    //     "contractOwnerName":"\u0634\u06cc\u0631\u06cc\u06a9",
    //     "contractNationalCode":"098776666444",
    //     "contractTel":"0930000001",
    //     "contractAddress":"wadwad",
    //     "articles":[
    //   {
    //     "goodsCode":125,
    //     "quantity":1,
    //     "amount":690000,
    //     "supplierCode":1,
    //     "purchaseArticleKind":"marketPlace",
    //     "inventoryCode":"2",
    //     "retailRevenue":"-",
    //     "retailTax":"-",
    //     "retailToll":"-",
    //     "commissionPrice":"0",
    //     "commissionRevenue":"0",
    //     "commissionTax":"0",
    //     "commissionToll":"-",
    //     "supplierShipping":"30000",
    //     "supplierShippingRevenue":"27523",
    //     "supplierShippingTax":"2477.07",
    //     "supplierShippingToll":"-",
    //     "inboundFee":"10000",
    //     "inboundFeeRevenue":"9174",
    //     "inboundFeeTax":"825.66",
    //     "inboundFeeToll":"-",
    //     "dealName":"\u0698\u0644 \u067e\u0627\u06a9 \u06a9\u0646\u0646\u062f\u0647 \u0648 \u0636\u062f\u0639\u0641\u0648\u0646\u06cc 120ml \u062f\u0633\u062a\u060c 5\u0639\u062f\u062f\u06cc Rafael",
    //     "supplierPayment":0
    //   }
    // ]
  },

    json: true
  };

  try{
    const parsedBody = await request(options);
    console.log(" parsedBody :" +  parsedBody.purchuseNo);
    const token = parsedBody.token;
    console.log(" token :" +  token)
    return token;
  } catch(err) {
    console.log(" err :" +  err);
  }

}

async function getStatus (){

  var options = {
    method: 'GET',
    uri: `http://localhost:5020/purchase/getStatus/${6}`,
    body: {
      // "id": "6",
    },

    json: true,
    preValidation:  `Bearer ${user}`,
    headers :{
      Authorization: `Bearer ${user}`
    },
  };


  try{
    const parsedBody = await request(options);
    console.log(" goodCh: :" +  parsedBody.supplier_checked +"-invche:"+ parsedBody.inventory_checked);
  } catch(err) {
    console.log(" err :" +  err);
  }

}

async function saveGoods (){

  var options = {
    method: 'GET',
    uri: `http://37.152.191.145:4020/goods/getGoods`,
    body: {
      // "id": "6",
    },

    json: true,
    preValidation:  `Bearer ${user}`,
    headers :{
      Authorization: `Bearer ${user}`
    },
  };


  try{
    const parsedBody = await request(options);
    for(let i=0; i<parsedBody.rows.length; i++) {
      const article = parsedBody.rows[i];

      var options2 = {
        method: 'POST',
        uri: 'http://37.152.191.145:4020/goods/create',
        body: {
          "id": article.id,
          "bookId" : article.bookId,
          "goodsCatId":article.goodsCatId,
          "code":article.code,
          "name":article.name,
          "unitId":article.unitId,
          "invalidFrom":article.invalidFrom,
          "calculationType":article.calculationType != null? article.calculationType : null,
        },

        json: true,
        preValidation:  `Bearer ${user}`,
        headers :{
          Authorization: `Bearer ${user}`
        },
      };

      const parsedBody2 = await request(options2);
    }
    console.log(" goods are: :" +  parsedBody.supplier_checked +"-invche:"+ parsedBody.inventory_checked);
  } catch(err) {
    console.log(" err :" +  err);
  }

}

async function saveReceipt (){

  const options = {
    method: 'GET',
    uri: `http://localhost:4020/goods/getGoods`,
    body: {
      // "id": "6",
    },

    json: true,
    preValidation:  `Bearer ${user}`,
    headers :{
      Authorization: `Bearer ${user}`
    },
  };


  try{
    const parsedBody = await request(options);
    let j=1;
    let bookId =  'f473b9f0-8be3-4ee6-9c08-f5b8e8862559';
    for(let i=0; i<parsedBody.rows.length; i++) {
      const article = parsedBody.rows[i];
      const { uuid } = require('uuidv4');
      // const Date = new Date()
      // const sec = new Date().getSeconds()
      // Date = new Date().setSeconds(sec+2)

      const options2 = {
        method: 'POST',
        timeoutSeconds: 10,
        uri: 'http://localhost:4020/receipt/create',
        body: {
          "id": uuid(),
          "bookId" : bookId,
          "receiptNo":j,
          "receiptDate":new Date(),
          "receiptType":"purchase",
          "inventoryId":"bef002d3-7af8-42c9-a3b9-c577145acb0f",
          "reference":"13990814",
          "explain":"خرید کالا",
          "articles":[
            {
              "id": uuid(),
              "goodsId":article.id,
              "quantity":1,
              "reference":article.code,
              "remark":"خرید کالا",
              "barcode":"1",
              "location":"5a5cc7f9-e551-4d77-ae75-0d96aba220ce",
            }
          ]

        },

        json: true,
        preValidation:  `Bearer ${user}`,
        headers :{
          Authorization: `Bearer ${user}`
        },
      };

      const parsedBody2 =  await request(options2);
      console.log(`number ${j}`, parsedBody2)
      // await sleep(500);
      j++;
    }
    console.log(" goods are: :" +  parsedBody.supplier_checked +"-invche:"+ parsedBody.inventory_checked);
  } catch(err) {
    console.log(" err :" +  err);
  }

}

async function saveSupplier (){

  try{
    const { uuid } = require('uuidv4');
    let bookId =  'f473b9f0-8be3-4ee6-9c08-f5b8e8862559';

      var options2 = {
        method: 'POST',
        uri: 'http://localhost:4020/contact/create',
        body: {
          "id": uuid(),
          "bookId" : bookId,
          "name":article.goodsCatId,
          "code":j,
        },

        json: true,
        preValidation:  `Bearer ${user}`,
        headers :{
          Authorization: `Bearer ${user}`
        },
      };

      const parsedBody2 = await request(options2);
    console.log(" goods are: :" +  parsedBody.supplier_checked +"-invche:"+ parsedBody.inventory_checked);
  } catch(err) {
    console.log(" err :" +  err);
  }

}

async function saveVoucher (){

  try{
    const { uuid } = require('uuidv4');
    let bookId =  'f473b9f0-8be3-4ee6-9c08-f5b8e8862559';

    var options2 = {
      method: 'POST',
      uri: 'http://localhost:4020/voucher/create',
      body: {
        "id": uuid(),
        "bookId" : bookId,
        "fiscalId" : "f473b9f0-8be3-4ee6-9c08-f5b8e8862553",
        "date" : new Date(),
        "seq" : "188",
        "remark" : "testAzi",
        "ref" : "refAzi",
        "voucherCat":"normal",
        "auto":true,
      },

      json: true,
      preValidation:  `Bearer ${user}`,
      headers :{
        Authorization: `Bearer ${user}`
      },
    };

    const parsedBody2 = await request(options2);
    console.log(" voucherSAaved");
  } catch(err) {
    console.log(" err :" +  err);
  }

}

async function saveArticle (){

  try{
    const { uuid } = require('uuidv4');
    let bookId =  'f473b9f0-8be3-4ee6-9c08-f5b8e8862559';

    var options2 = {
      method: 'POST',
      uri: 'http://localhost:4020/article/create',
      body: {
        "id": uuid(),
        "bookId" : bookId,
        "voucherId" : "4d9c61b3-2a23-4b2a-a1b4-dfc856b23749",
        "seq" : "1",
        "accId" : "f473b9f0-8be3-4ee6-9c08-f5b8e8862551",
        // "contactId" : NULL,
        // "contactCatId" : "refAzi",
        // "auxIds":"normal",
        "amount":"1000",
        // "vatProps":true,
        // "extendedProps":true,
        "ref":"testA",
        "remark":"testAzi",
        // "currencyId":true,
        // "effectiveDate":true,
        // "currencyAmount":true,
      },

      json: true,
      preValidation:  `Bearer ${user}`,
      headers :{
        Authorization: `Bearer ${user}`
      },
    };

    const parsedBody2 = await request(options2);
    console.log(" voucherSAaved");
  } catch(err) {
    console.log(" err :" +  err);
  }

}

async function issueBillVoucher (){
  var optionsGet = {
    method: 'GET',
    uri: `http://localhost:4020/bill/getBills`,
    body: {
      // "id": "6",
    },

    json: true,
    preValidation:  `Bearer ${user}`,
    headers :{
      Authorization: `Bearer ${user}`
    },
  };


  try{
    const parsedBodyGet = await request(optionsGet);
    for(let i=0; i<parsedBodyGet.rows.length; i++) {
      const bill = parsedBodyGet.rows[i];

      const { uuid } = require('uuidv4');
      let bookId =  'f473b9f0-8be3-4ee6-9c08-f5b8e8862559';
      var options = {
        timeoutSeconds: 10,
        method: 'POST',
        uri: 'http://localhost:4020/voucher/create',
        body: {
          "id": uuid(),
          "bookId": bookId,
          "fiscalId": "f473b9f0-8be3-4ee6-9c08-f5b8e8862553",
          "date": bill.bill_date,
          "seq": "188",
          "remark": bill.remark,
          "ref": bill.reference,
          "voucherCat": "normal",
          "auto": true,
        },

        json: true,
        preValidation: `Bearer ${user}`,
        headers: {
          Authorization: `Bearer ${user}`
        },
      };

      const parsedBody = await request(options);
      console.log("headerSave");

      var options2 = {
        method: 'POST',
        uri: 'http://localhost:4020/article/create',
        body: {
          "id": uuid(),
          "bookId": bookId,
          "voucherId": parsedBody.id,
          "seq": "1",
          "accId": "f473b9f0-8be3-4ee6-9c08-f5b8e8862551",
          // "contactId" : NULL,
          // "contactCatId" : "refAzi",
          // "auxIds":"normal",
          "amount": bill.amount,
          // "vatProps":true,
          // "extendedProps":true,
          "ref": "testA",
          "remark": bill.billremark,
          // "currencyId":true,
          // "effectiveDate":true,
          // "currencyAmount":true,
        },

        json: true,
        preValidation: `Bearer ${user}`,
        headers: {
          Authorization: `Bearer ${user}`
        },
      };

      const parsedBody2 = await request(options2);
      console.log(" articleSaved");
    }
    console.log(" billVoucherd");

  } catch(err) {
    console.log(" err :" +  err);
  }

}

async function getAccKamand (){

  var options = {
    method: 'GET',
    uri: `http://localhost:8050/acc/6`,
    body: {
      "id": "6",
    },

    json: true,
    preValidation:  `Bearer ${user}`,
    headers :{
      Authorization: `Bearer ${user}`
    },
  };


  try{
    const parsedBody = await request(options);
    console.log(" acc: :" +  parsedBody.id);
  } catch(err) {
    console.log(" err :acc" +  err);
  }

}

async function callQuery (){
  let bookId =  'f473b9f0-8be3-4ee6-9c08-f5b8e8862559';
  var options = {
    method: 'GET',
    uri: `http://localhost:4020/private/data/activity_type_list?bookId=f473b9f0-8be3-4ee6-9c08-f5b8e8862559`,

    json: true,
    preValidation:  `Bearer ${user}`,
    headers :{
      Authorization: `Bearer ${user}`
    },
  };


  try{
    const parsedBody = await request(options);
    console.log(" acc: :" +  parsedBody.id);
  } catch(err) {
    console.log(" err :acc" +  err);
  }
}

async function createSupply (){

  var options = {
    method: 'POST',
    uri: 'http://localhost:5020/supplyRequest/create',
    body: {
      "supplyRequestNo": "p2",
      "supplyRequestDate": "2020-10-20 23:33:03",
      "supplyRequestKind": "retail",
      "supplierCode": "001",
      "attorneyCode": "001",
      "articles": [
        {
          "quantity": "1",
          "purchaseNo": "sh-600",
        }
      ]
    },

    json: true,
    preValidation:  `Bearer ${user}`,
    headers :{
      Authorization: `Bearer ${user}`
    },

  };

  try{
    const parsedBody = await request(options);
    console.log(" parsedBody :" +  parsedBody.purchuseNo);
    const token = parsedBody.token;
    console.log(" token :" +  token)
    return token;
  } catch(err) {
    console.log(" err :" +  err);
  }
}

async function saveGoodsByOMS (){

  var options = {
    method: 'POST',
    uri: 'http://localhost:5020/goods/create',
    body: {
      "goodsCatCode":"001",
      "code":"testOMS03",
      "name":"testOMS3",
      "calculationType":"Reference",
    },


    json: true,
    preValidation:  `Bearer ${user}`,
    headers :{
      Authorization: `Bearer ${user}`
    },
  };


  try{
    const parsedBody = await request(options);
    console.log(" parsedBody :" +  parsedBody.purchuseNo);
    const token = parsedBody.token;
    console.log(" token :" +  token)
    return token;
  } catch(err) {
    console.log(" err :" +  err);
  }


}

async function findGoodsByOMS (){

  var options = {
    method: 'GET',
    uri: `http://localhost:5020/goods/findByCode/${10010010}`,
    body: {
      "code":"testOMS",
    },


    json: true,
    preValidation:  `Bearer ${user}`,
    headers :{
      Authorization: `Bearer ${user}`
    },
  };


  try{
    const parsedBody = await request(options);
    console.log(" parsedBody :" +  parsedBody.purchuseNo);
    const token = parsedBody.token;
    console.log(" token :" +  token)
    return token;
  } catch(err) {
    console.log(" err :" +  err);
  }


}

async function getGoods (){

  var options = {
    method: 'GET',
    uri: `http://localhost:5020/goods/getGoods`,
    body: {
      "code":"testOMS",
    },


    json: true,
    preValidation:  `Bearer ${user}`,
    headers :{
      Authorization: `Bearer ${user}`
    },
  };


  try{
    const parsedBody = await request(options);
    console.log(" parsedBody :" +  parsedBody.purchuseNo);
    const token = parsedBody.token;
    console.log(" token :" +  token)
    return token;
  } catch(err) {
    console.log(" err :" +  err);
  }


}

async function createPick (){

  var options = {
    method: 'POST',
    uri: 'http://localhost:5020/pick/create',
    body: {
      "pickNo": "pick1",
      "pickDate": "2020-10-20 23:33:03",
      "operCode": "001",
      "inventoryCode": "001",
      "articles": [
        {
          "purchaseNo": "sh-700",
        }
      ]
    },
    // body: {
    //   "id": "c00b90e0-09ca-4900-9489-a11e41843075",
    //   "pickNo": "pick1",
    //   "pickDate": "2020-10-20 23:33:03",
    //   "operCode": "001",
    //   "inventoryCode": "001",
    //   "articles": [
    //     {
    //       "id": "9d3f8540-8e10-4892-9a15-38d96be8c6a5",
    //       "purchaseNo": "sh-700",
    //       "ref": "108",
    //       "locationCode": "3434",
    //     }
    //   ]
    // },

    json: true,
    preValidation:  `Bearer ${user}`,
    headers :{
      Authorization: `Bearer ${user}`
    },

  };

  try{
    const parsedBody = await request(options);
    console.log(" parsedBody :" +  parsedBody);
  } catch(err) {
    console.log(" err :" +  err);
  }
}

async function createPack (){

  var options = {
    method: 'POST',
    uri: 'http://localhost:5020/pick/create',
    body: {
      "packNo": "pack1",
      "packDate": "2020-10-20 23:33:03",
      "operCode": "001",
      "inventoryCode": "001",
      "articles": [
        {
          "purchaseNo": "sh-700",
          "ref": "test",
        }
      ]
    },

    json: true,
    preValidation:  `Bearer ${user}`,
    headers :{
      Authorization: `Bearer ${user}`
    },

  };

  try{
    const parsedBody = await request(options);
    console.log(" parsedBody :" +  parsedBody.purchuseNo);
    const token = parsedBody.token;
    console.log(" token :" +  token)
    return token;
  } catch(err) {
    console.log(" err :" +  err);
  }
}

async function main(){
  await login();
  const token = await createPick();
  console.log("after token :" +  token);
}
main();