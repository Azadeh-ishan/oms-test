const fastify = require('fastify')()



fastify.register(require('fastify-swagger'), {
    routePrefix: '/documentation',
    swagger: {
        info: {
            title: 'Test swagger',
            description: 'testing the fastify swagger api',
            version: '0.1.0'
        },
        externalDocs: {
            url: 'https://swagger.io',
            description: 'Find more info here'
        },
        host: 'localhost',
        schemes: ['http'],
        consumes: ['application/json'],
        produces: ['application/json'],
        tags: [
            { name: 'user', description: 'User related end-points' },
            { name: 'code', description: 'Code related end-points' }
        ],
        definitions: {
            User: {
                $id: 'User',
                type: 'object',
                required: ['id', 'email'],
                properties: {
                    id: { type: 'string', format: 'uuid' },
                    firstName: { type: 'string', nullable: true },
                    lastName: { type: 'string', nullable: true },
                    email: {type: 'string', format: 'email' }
                }
            }
        },
        securityDefinitions: {
            apiKey: {
                type: 'apiKey',
                name: 'apiKey',
                in: 'header'
            }
        }
    },
    exposeRoute: true,
    routePrefix: '/documentations'//todo its for overwrite the /documentation url
})

fastify.put('/some-route/:id', {
    schema: {
        description: 'post some data',
        tags: ['user', 'code'],
        summary: 'qwerty',
        params: {
            type: 'object',
            properties: {
                id: {
                    type: 'string',
                    description: 'user id'
                }
            }
        },
        body: {
            type: 'object',
            properties: {
                hello: { type: 'string' },
                obj: {
                    type: 'object',
                    properties: {
                        some: { type: 'string' }
                    }
                }
            }
        },
        response: {
            201: {
                description: 'Successful response',
                type: 'object',
                properties: {
                    hello: { type: 'string' }
                }
            }
        },
        security: [
            {
                "apiKey": []
            }
        ]
    }
}, (req, reply) => {})

fastify.ready(err => {
    if (err) throw err
    fastify.swagger()
})

